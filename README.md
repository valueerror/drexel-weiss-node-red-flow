# Drexel und Weiss - Node-Red Flow


This Node-Red flow creates all sensors and controlbuttons in HomeAssistant and updates all of the values in a 4 minute interval.
It handles the output of the USB Serial interface of a Drexel&Weiss x² plus Heatpump and does all of the necessary value transformations. 
Node-Red should be installed on a raspberry-pi zero directly connected to the x² device.

In order to get all of the Node-Red nodes used in this flow you have to install **node-red-contrib-ha-mqtt** (v1.2.10).

It broadcasts all of the information via mqtt so you have to install **mosquitto** addon in HomeAssistant.

![dashboard](https://gitlab.com/valueerror/drexel-weiss-node-red-flow/-/raw/main/x2-dashboard.png)

![flow](https://gitlab.com/valueerror/drexel-weiss-node-red-flow/-/raw/main/x2-nodered-flow.jpg)


